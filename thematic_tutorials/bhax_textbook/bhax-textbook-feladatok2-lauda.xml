<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Lauda!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Port scan</title>
        <para>
            Mutassunk rá ebben a port szkennelő forrásban a kivételkezelés szerepére!
        </para>
        <para>
            <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html#id527287">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html#id527287</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/botond201/bhax/tree/master/laudasource">https://gitlab.com/botond201/bhax/tree/master/laudasource</link>               
        </para>
        <para>
            A port scan program célja, hogy gépünk portjai közül, egy néhány sor segítségével megnézzük, hogy melyek szabadok, és melyek vannak már használatban.
            Ezt socketekkel és egy <command>try-catch</command> mechanizmussal valósítjuk meg.
        </para>
        <para>
            <programlisting language="java"><![CDATA[
public class PortScan {
    
    public static void main(String[] args) {

	int figyelt = 0;
	int nemfigyelt = 0;
        
        for(int i=0; i<1024; ++i)
            
            try {
                
                java.net.Socket socket = new java.net.Socket(args[0], i);
                
                System.out.println(i + " figyeli");

		figyelt++;
                
                socket.close();
                
            } catch (Exception e) {
                
                System.out.println(i + " nem figyeli");

		nemfigyelt++;
                
            }
	System.out.println("Figyelt portok száma: " + figyelt);

	System.out.println("Nem figyelt portok száma: " + nemfigyelt);
    }
    
}]]></programlisting>
        </para>
        <para>
            Az eredeti programba két számlálót is beépítettem, hogy könnyebben át lehessen látni a portok állapotát.
        </para>
        <para>
            A fordítás és futtatás egyszerű:
            <programlisting language="c++"><![CDATA[javac PortScan.java
java PortScan]]></programlisting>
        </para>
        <figure>
                <title>PortScan</title>
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="../../laudasource/portscan.png" format="PNG"/>
                        </imageobject>
                    </mediaobject>
        </figure>
        <para>
            Hogyan működik ez az egyszerű program?
        </para>
        <para>
            Egy egyszerű <command>for</command> ciklussal végig megyünk a számokon 1024-ig. Ezek képviselik a portokat.
            Majd minden számra egy socketet próbálunk létrehozni. Ez van a <command>try</command> részben. Ha sikerül, akkor értelemszerűen 
            az a port szabad volt, mert létrejött a kapcsolat.
        </para>
        <para>
            Majd itt jön be a <command>catch</command> szerepe. Nyilván, ha nem sikerül kapcsolatot létesíteni, akkor az hibát fog dobni. 
            Ezt a "dobást kapja el" a catch. Tehát az a port már foglalt. Ezért került a vezérlés a hibát követően a <command>catch</command> ágba.
        </para>
    </section>        

    <section>
        <title>AOP</title>
        <para>
            Szőj bele egy átszövő vonatkozást az első védési programod Java átiratába! (Sztenderd védési
feladat volt korábban.)
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/botond201/bhax/tree/master/laudasource">https://gitlab.com/botond201/bhax/tree/master/laudasource</link>               
        </para>
        <para>
            Az AOP (Aspect Oriented Programming) egy programozási paradigma célja, hogy növelje a modularitást.
            Ezt úgy éri el, hogy egy létező kódhoz további viselkedést ad hozzá, anélkül, hogy azt módosítaná.
            Inkább külön specifikál egy vágási pontot <command>(pointcut)</command> ahol "életbe lépnek" a megadott "extra" utasítások.
        </para>
        <para>
            Ezt kell itt is alkalamznunk. Ezt itt az AspectJ segítségével fogjuk bemutatni.
        </para>
        <para>
            Először lássunk egy kis példát.
            Vegyük először az alap java programot:
            <programlisting language="java"><![CDATA[
public class Test
{
	public static class testtest
	{
		public void testfn()
		{
			System.out.println("test");
		}
	}
	
	public static void main(String[] args)
	{
		testtest test1 = new testtest();
		test1.testfn();
	}
}]]></programlisting>
        </para>
        <para>
            A működés nagyon egyszerű. Létrejön egy teszt osztály, aminek egyetlen teszt függvényét 
            hívjuk meg, ami kiír egy egyszerű szöveges üzenetet a képernyőre.
        </para>
        <para>
            Majd következik az AOP-s szövés, egy aspectj file képében:
            <programlisting language="java"><![CDATA[
public aspect BeforeAfter
{
	public pointcut fnCall(): call(public void testfn());

	before(): fnCall() 
	{
		System.out.println("before> test");
	}

	after(): fnCall()
	{
		System.out.println("after> test");
	}
}]]></programlisting>
        </para>
        <para>
            Létrehozunk egy aspect-et, melyben történik a lényeg. A <command>pointcut</command> a vágási pontot jelöli, azaz 
            ha az ott megjelölt függvényhez érünk az eredeti programban, akkor fog ez végrehajtódni. 
            Itt konkrétan az eredeti függvény futásakor elé és utána egy kis nyomkövető üzenetet írunk ki.
        </para>
        <para>
            És akkor hogy néz ez ki?
        </para>
        <para>
            Először szükségünk lesz az aspectj-re: <command>sudo apt install aspectj</command>.
        </para>
        <para>
            Majd fordítjuk, és futtatásnál pedig megadjuk a megfelelő aspectj könyvtárat.
            <programlisting language="java"><![CDATA[
ajc Test.java BeforeAfter.aj
java -cp /usr/share/java/aspectjrt.jar:. Test]]></programlisting>
        </para>
        <figure>
                <title>Eredmény</title>
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="../../laudasource/aop.png" format="PNG"/>
                        </imageobject>
                    </mediaobject>
        </figure>
        <para>
            Majd nézzük az eredeti feladatot. Az alap az LZWBinFa program java átirata. Ezt már ebben a félévben egy korábbi feladatban elkészítettük
        </para>
        <para>
            Az eredeti BinFa programot kicsit módosítottam, a jobb láthatóság érdekében. Most nem fájlba írjuk az eredményünket, hanem csak 
            egyszerűen a képernyőre.
        </para>
        <para>
            Ebben az esetben a nyomkövetésünk nagyon egyszerű:
            <programlisting language="java"><![CDATA[privileged aspect Binfa{

    public pointcut atlag(): call(public double getAtlag ());

    after(): atlag()
    {
        System.out.println("--- Átlagszámítás ---");
    }
}]]></programlisting>
        </para>
        <para>
            Annyi történik, hogy bármikor a BinFa program futása során meghívódik a <function>getAtlag()</function> függvény, akkor 
            utána egy egyszerű nyomkövető sort szúrunk be.
        </para>
        <figure>
                <title>Átlag nyomkövetés</title>
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="../../laudasource/binfaaj.png" format="PNG"/>
                        </imageobject>
                    </mediaobject>
        </figure>
        <para>
            Mit látunk?
        </para>
        <para>
            A futás során az átlagszámító függvényt kétszer is alkalmazzuk, egyszer az átlag kiszámításánal, máskor pedig a szórás 
            kiszámításánál.
        </para>
    </section>  
    
    <section>
        <title>Android játék</title>
        <para>
            Írjunk egy egyszerű Androidos „játékot”! Építkezzünk például a 2. hét „Helló, Android!” feladatára!
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/botond201/bhax/tree/master/laudasource/android">https://gitlab.com/botond201/bhax/tree/master/laudasource/android</link>               
        </para>
        <para>
            Használt eszköz: Android Studio. Az app tesztelésekor saját telefont (Huawei Honor Play) használtam.
        </para>
        <para>
            A projekt létrehozásakor Az empty activity-vel kezdtem dolgozni. A választott android verzió: 4.4 (KitKat). Azért ezt választottam, 
            mert így az app az eszközök bő 90 százalékán fut.
        </para>
        <para>
            Majd elkészítettem a projektet, ami legenerálta nekem az alapforrásokat. Ezekből 2 állomnyra lesz szükségünk. Az objektumokat leíró 
            xml állományra, és a programot működtető java fájlra.
        </para>
        <para>
            Az ötlet egyszerű. Sok buli alapja a felelsz vagy mersz. Ehhez sokszor alap szokott lenni egy tárgy amellyel ki lehet sorsolni, 
            hogy ki tegyen fel kérdést kinek. Ez gyakran egy üveg vagy flakon szokott lenni.
        </para>
        <para>
            Az app pedig egyszerűen ezt váltja ki. Egy egyszerű háttéren egy üveget lehet megpörgetni, amely így egy véletlenszerű irányt fog felvenni.
        </para>
        <para>
            Kettő képre van szükségünk. Első a háttér. Erre egy egyszerű faanyag-hátteret választottam, hogy azt a hatást keltse, hogy vagy egy asztalon, vagy 
            egy padlón vagyunk. A másik az üveg képe, amit pörgetni fogunk. Ez kicsit sörösüvegre hajaz, erősítve a buli-hangulatot.
        </para>
        <para>
            Ezeket a képeket behúzzuk a projekt <command>resource</command> mappájába.
        </para>
        <para>
            Majd elkészítjük az <command>activity_main.xml</command>-t ami ezeket fogja leírni a programunk számára:
            <programlisting language="xml"><![CDATA[
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@drawable/floor2"
    tools:context="com.botond.bottlespin.MainActivity">

    <ImageView
        android:id="@+id/bottle"
        android:layout_width="300dp"
        android:layout_height="300dp"
        android:src="@drawable/bottle"
        android:layout_centerInParent="true"
        android:onClick="spinBottle"/>

</RelativeLayout>]]></programlisting>
        </para>
        <para>
            Ebben megadjuk a háttér alapjául szóló képet, hozzárendelünk egy azonosító, megadjuk a méreteit. Illetve 
            megadjuk a palack azonosítóját, a méretét, a pozícióját, és a rákattintáskor végrehajtandó metódus nevét.
        </para>
        <para>
            Majd pedig jön a <command>MainActivity.java</command>:
            <programlisting language="java"><![CDATA[
package com.botond.bottlespin;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ImageView bottle;
    private Random random = new Random();
    private int lastDir;
    private boolean spinning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottle = findViewById(R.id.bottle);
    }

    public void spinBottle(View v) {
        if (!spinning) {
            int newDir = random.nextInt(900) + 900;
            float pivotX = bottle.getWidth() / 2;
            float pivotY = bottle.getHeight() / 2;

            Animation rotate = new RotateAnimation(lastDir, newDir, pivotX, pivotY);
            rotate.setDuration(3000);
            rotate.setFillAfter(true);
            rotate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    spinning = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    spinning = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            lastDir = newDir;
            bottle.startAnimation(rotate);
        }
    }
}
]]></programlisting>
        </para>
        <para>
            Az importált konyvtárak, szükséges változók és az indítási alapfüggvény után láthatjuk a pörgetést végző metódust.
            Ebben egy új írányt adunk az üvegnek egy 900 és 1800 közötti random szám segítségével. Meg is adhatjuk a pörgés sebességét.            
        </para>
        <para>
            És hogy néz ki az eredmény?
        </para>
        <figure>
                <title>BotleSpin app</title>
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="../../laudasource/android/bottlespin.png" format="PNG"/>
                        </imageobject>
                    </mediaobject>
        </figure>
    </section>
    
    <section>
        <title>Junit teszt</title>
        <para>
            A <link xlink:href="https://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedat">https://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedat</link>
poszt kézzel számított mélységét és szórását dolgozd be egy Junit tesztbe (sztenderd védési feladat
volt korábban).
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/botond201/bhax/tree/master/laudasource/junit">https://gitlab.com/botond201/bhax/tree/master/laudasource/junit</link>               
        </para>
        <para>
            A JUnit egy Java-hoz készített tesztrendszer. Lényegében a kód mellé készítünk egy teszt osztályt, ami az abban elvégzett metódusok 
            tesztelésére szolgál.
        </para>
        <para>
            A feladatunk megint az <command>LZWBinFa.java</command>-val kapcsolatos. Most az említett cikkben kiszámolt adatokat kell ellenőriznünk, 
            hogy azok helyesek-e. 
        </para>
        <para>
            Ezt a feladatot az Eclipse segítségével könnyen el lehet végezni.
        </para>
        <para>
            Létrehozunk benne egy java projektet, majd abban egy package-et. Ebben létrehozzuk az <command>LZWBinFa.java</command> java classt, majd beletesszük 
            a kódunkat, amit akár az <function>Arroway</function> fejezetben is láthatunk. Ezt követően létrehozunk egy
            <command>Junit Test case</command>-t amiben a tesztünket fogjuk megírni.
        </para>
        <para>
            A teszt a következő:
            <programlisting language="java"><![CDATA[
package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LZWBinFaTest {

	LZWBinFa binfa = new LZWBinFa();
	
	@Test
	void test() {
		String word = "01111001001001000111";
		for(int i=0;i<word.length();i++) {
			if(word.charAt(i) == '1') {
				binfa.kovetkezobit('1');
			}else binfa.kovetkezobit('0');
		}
		
		assertEquals(2.75, binfa.getAtlag());
		assertEquals(4, binfa.getMelyseg());
		assertEquals(0.9574, binfa.getSzoras(), 0.0001);
		
	}

}
]]></programlisting>
        </para>
        <para>
            Szinte majdnem egy java class. De mit is csinálunk itt?
        </para>
        <para>
            Először példányosítunk egy LZWBinFa objektumot, hogy aztán azzal tudjunk dolgozni.
            Majd következik a <command>@Test</command>. Erről fogja tudni a program, hogy innentől következik a teszt.
            A tesztben a mintát elhelyezzük egy sztringben. Majd egy egyszerű for ciklussal karakterenként belevisszük a fába, 
            a megfelelő függvényünkkel.                       
        </para>
        <para>
            Ezt követi a valódi teszt. Az <function>assertEqual()</function> függvény segítségével hasonlítjuk össze a kézzel kapott értékeket, 
            és a függvény által számolt értékeket.
        </para>
        <para>
            Ezen függvény harmadik paramétere arra jó, hogy megadjuk azt a tizedes pontosságot, ameddig végezni akarjuk az összehasonlítást.
        </para>
        <para>
            Majd ha kész vagyunk, akkor jobb klikk a testre, és teszt futtatása. Ezzel le is fut a tesztünk.
        </para>
        <figure>
                <title>JUnit teszt</title>
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="../../laudasource/junit/junit.png" format="PNG"/>
                        </imageobject>
                    </mediaobject>
        </figure>
        <para>
            Bal oldalon a zöld sáv értesít minket arról, hogy a tesztünk sikeres volt. Ez azt jelenti, hogy a java kódban implementált függvényeink 
            jól működnek.
        </para>
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
</chapter>                
