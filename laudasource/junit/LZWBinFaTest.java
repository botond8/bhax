package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LZWBinFaTest {

	LZWBinFa binfa = new LZWBinFa();
	
	@Test
	void test() {
		String word = "01111001001001000111";
		for(int i=0;i<word.length();i++) {
			if(word.charAt(i) == '1') {
				binfa.kovetkezobit('1');
			}else binfa.kovetkezobit('0');
		}
		
		assertEquals(2.75, binfa.getAtlag());
		assertEquals(4, binfa.getMelyseg());
		assertEquals(0.9574, binfa.getSzoras(), 0.0001);
		
	}

}
