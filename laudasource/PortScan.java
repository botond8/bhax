public class PortScan {
    
    public static void main(String[] args) {

	int figyelt = 0;
	int nemfigyelt = 0;
        
        for(int i=0; i<1024; ++i)
            
            try {
                
                java.net.Socket socket = new java.net.Socket(args[0], i);
                
                System.out.println(i + " figyeli");

		figyelt++;
                
                socket.close();
                
            } catch (Exception e) {
                
                System.out.println(i + " nem figyeli");

		nemfigyelt++;
                
            }
	System.out.println("Figyelt portok száma: " + figyelt);

	System.out.println("Nem figyelt portok száma: " + nemfigyelt);
    }
    
}
