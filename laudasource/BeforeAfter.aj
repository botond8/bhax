public aspect BeforeAfter
{
	public pointcut fnCall(): call(public void testfn());

	before(): fnCall() 
	{
		System.out.println("before> test");
	}

	after(): fnCall()
	{
		System.out.println("after> test");
	}
}
