#include <iostream>

class Szam{
private: int* ertek;
public: 
    Szam(int x){
	ertek= new int(x);
    std::cout<<"konstruktor"<<std::endl;
    }

    ~Szam(){
    delete ertek;
    std::cout<<"destruktor"<<std::endl;
    }
    
    Szam (const Szam & x) : ertek (new int) {
        *ertek=*(x.ertek);
        std::cout<<"másoló konstruktor"<<std::endl;
    }
    
    Szam& operator= (const Szam & n){
        int *uj_ertek = new int();
        *uj_ertek = *(n.ertek);
        delete ertek;
        ertek=uj_ertek;
        std::cout<<"másoló értékadás"<<std::endl;
        return *this;
    }
    
    Szam (Szam && n) : ertek (nullptr){
        *this = std::move(n);
        std::cout<<"mozgató konstruktor"<<std::endl;
    }
    
    Szam& operator= (Szam && n){
        std::swap(ertek,n.ertek);
        std::cout<<"mozgató értékadás"<<std::endl;
        
        return *this;
    }
};

int main(){
    Szam egy(1);
    Szam ketto(2);
    Szam harom(egy);
    ketto=egy;
    harom=std::move(egy);
    Szam negy=std::move(ketto);
}
