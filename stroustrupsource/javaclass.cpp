#include <iostream>
#include <string>
#include <boost/filesystem.hpp>

class JDK
{
private:
    int classcounter = 0;
public:
    int getclasscounter()
    {
        return classcounter;
    }
    void kereso ( boost::filesystem::path path)
    {

        if ( is_regular_file ( path ) ) {

                std::string ext ( ".java" );
                if ( !ext.compare ( boost::filesystem::extension ( path ) ) ) {
                    std::cout<<path<<std::endl;
                }
                classcounter++;

        } else if ( is_directory ( path ) )
                for ( boost::filesystem::directory_entry & entry : boost::filesystem::directory_iterator ( path ) )
                        kereso ( entry.path());

    }
};    
int main()
{
    JDK jdk;
    std::cout<<"Search"<<std::endl;
    jdk.kereso("src");
    std::cout<<"Number of found classes: "<< jdk.getclasscounter()<<std::endl;
}
