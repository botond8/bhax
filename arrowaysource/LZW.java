package botond.lzwservlet.servlets;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LZW
 */
@WebServlet("/LZW")
public class LZW extends HttpServlet {
	
	public LZW ()
    {
		fa = gyoker;
    }

    public void kovetkezobit(char b)
    {
        
        if (b == '0')
        {
            
            if (fa.nullasGyermek() == null)	
            {  
                Csomopont uj = new Csomopont ('0');
                fa.ujNullasGyermek (uj);
                fa = gyoker;
            }
            else			
            {
                
                fa = fa.nullasGyermek ();
            }
        }
        
        else
        {
            if (fa.egyesGyermek() == null)
            {
                Csomopont uj = new Csomopont('1');
                fa.ujEgyesGyermek(uj);
                fa = gyoker;
            }
            else
            {
                fa = fa.egyesGyermek();
            }
        }
    }
    class Csomopont
    {
        public Csomopont (char betu)
        {
			this.betu = betu;
			balNulla = null;
			jobbEgy = null;
        };
       
        public Csomopont nullasGyermek()
        {
            return balNulla;
        }
        
        Csomopont egyesGyermek()
        {
            return jobbEgy;
        }
        
        public void ujNullasGyermek (Csomopont gy)
        {
            balNulla = gy;
        }
        
        public void ujEgyesGyermek (Csomopont gy)
        {
            jobbEgy = gy;
        }
        
        public char getBetu ()
        {
            return betu;
        }
        
        private char betu;
        
        private Csomopont balNulla;
        private Csomopont jobbEgy;
    }

    private Csomopont fa = null;
    
    private int melyseg, atlagosszeg, atlagdb;
    private double szorasosszeg;
		
    protected Csomopont gyoker = new Csomopont('/');
    int maxMelyseg;
    double atlag, szoras;
	
	public int getMelyseg ()
{
    melyseg = maxMelyseg = 0;
    rmelyseg (gyoker);
    return maxMelyseg - 1;
}

public double getAtlag ()
{
    melyseg = atlagosszeg = atlagdb = 0;
    ratlag (gyoker);
    atlag = ((double) atlagosszeg) / atlagdb;
    return atlag;
}

public double getSzoras ()
{
    atlag = getAtlag ();
    szorasosszeg = 0.0;
    melyseg = atlagdb = 0;

    rszoras (gyoker);

    if (atlagdb - 1 > 0)
        szoras = Math.sqrt (szorasosszeg / (atlagdb - 1));
    else
        szoras = Math.sqrt (szorasosszeg);

    return szoras;
}

public void rmelyseg (Csomopont elem)
{
    if (elem != null)
    {
        ++melyseg;
        if (melyseg > maxMelyseg)
            maxMelyseg = melyseg;
        rmelyseg (elem.egyesGyermek ());
        
        rmelyseg (elem.nullasGyermek ());
        --melyseg;
    }
}

public void ratlag (Csomopont elem)
{
    if (elem != null)
    {
        ++melyseg;
        ratlag (elem.egyesGyermek ());
        ratlag (elem.nullasGyermek ());
        --melyseg;
        if (elem.egyesGyermek () == null && elem.nullasGyermek () == null)
        {
            ++atlagdb;
            atlagosszeg += melyseg;
        }
    }
}

public void rszoras (Csomopont elem)
{
    if (elem != null)
    {
        ++melyseg;
        rszoras (elem.egyesGyermek ());
        rszoras (elem.nullasGyermek ());
        --melyseg;
        if (elem.egyesGyermek () == null && elem.nullasGyermek () == null)
        {
            ++atlagdb;
            szorasosszeg += ((melyseg - atlag) * (melyseg - atlag));
        }
    }
}
    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
         String befile = "pHRqF6JX6pBGATGIS9nYT5gs6H0VU60BP8q3WKB7";
         PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("o")));
         byte[] b = befile.getBytes();
         LZW binFa = new LZW();
         response.setContentType("text/html");
         boolean kommentben = false;
         for(int i=0; i<b.length; i++)
         {
             if (b[i] == 0x3e) 
             {			
                 kommentben = true;
                 continue;
             }
             if (b[i] == 0x0a) 
             {			
                 kommentben = false;
                 continue;
             }
             if (kommentben)
             {
                 continue;
             }
             if (b[i] == 0x4e)
             {
                 continue;
             }
             for (int j = 0; j < 8; ++j)
             {
                 if ((b[i] & 0x80) != 0)
                 {
                     binFa.kovetkezobit('1');
                 } 
                 else
                 {
                     binFa.kovetkezobit('0');
                 }
                 b[i] <<= 1;
             }
         }

         response.setContentType("text/html");                  
         
         output.print("Mélység: " + binFa.getMelyseg() + "\n");
         output.print("Átlag: " + binFa.getAtlag () + "\n");
         output.print("Szórás: " + binFa.getSzoras () + "\n");

         output.close();
         PrintWriter out = response.getWriter();

         
         out.println("<html>");
         out.println("<head><title>LZWBinFa</title></head>");
         out.println("<body>");
         out.println("<h1 align='center'>Binfa</h1>");
         
         try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("o")))))
         {
             for(String line; (line = br.readLine()) != null; ) 
             {
                 out.println("<p>" + line + "</p>");
             }
         }

         
         out.println("</body></html>");
         
	}

};
