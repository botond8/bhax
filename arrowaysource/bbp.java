import java.lang.*;
import java.util.*;

class bbp
{
	public static long n16modk(int n, int k)
	{
		int t = 1;
		while(t <= n)
		{
			t =t*2;
		}
		long r = 1;
		while(true)
		{
			if(n >= t)
			{
				r = (16*r) % k;
				n = n - t;
			}
			t = t/2;
			if(t < 1)
			{
				break;
			}
			r = (r*r) % k;
		}
		return r;
	}
	public static double d16Sj(int d, int j)
	{
		double szam = 0.0d;
		for(int i=0;i<=d;++i)
		{
			szam += n16modk(d-i, 8*i+j) / (double)(8*i+j);
		}
		for(int i=d+1;i<=2*d;++i)
		{
			szam += StrictMath.pow(16.0d ,d-i) / (double)(8*i+j);
		}
		return szam - StrictMath.floor(szam);
	}
	
	public static void main(String args[])
	{
		int d = 1000000;
		StringBuffer buffer = new StringBuffer();
		
		Character hexa[] = {'A','B','C','D','E','F'};
		
		double d16pi;
		
		double d16S1 = d16Sj(d,1);
		double d16S4 = d16Sj(d,4);
		double d16S5 = d16Sj(d,5);
		double d16S6 = d16Sj(d,6);
		
		d16pi = (4*d16S1 - 2*d16S4 - d16S5 - d16S6) - StrictMath.floor(4*d16S1 - 2*d16S4 - d16S5 - d16S6);
		
		while(d16pi != 0.0d)
		{
			int szjegy = (int)StrictMath.floor(d16pi * 16.0d);
			
			if(szjegy<10)
			{
				buffer.append(szjegy);
			}
			if(szjegy>=10)
			{
				buffer.append(hexa[szjegy-10]);
			}
			
			d16pi = (d16pi * 16.0d) - StrictMath.floor(d16pi * 16.0d);
		}
		
		System.out.println(buffer);
	}
}
