#include <stdio.h>

int main()
{
	printf("Segédváltozós csere\n\n");
	int a=5;
	int b=7;
	printf("Eredeti értékek: a=%d b=%d\n",a,b);
	int c=a;
	a=b;
	b=c;
	printf("Felcserélve: a=%d b=%d\n",a,b);

	return 0;
}
