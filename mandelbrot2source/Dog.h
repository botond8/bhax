#ifndef DOG_H
#define DOG_H

class Dog : Animal {

private:
	Boolean loyal;
	Boolean pref_indoor;

public:
	void PlaywithMaster();

	void dig();
};

#endif
