#ifndef ANIMAL_H
#define ANIMAL_H

class Animal : Duck {

private:
	Int age;
	String gender;

public:
	void eat();

	void sleep();

	void reproduce();
};

#endif
