#ifndef FISH_H
#define FISH_H

class Fish : Animal {

private:
	Int size;
	Boolean edible;

public:
	void swim();

	void BreathUnderwater();
};

#endif
