class Szulo
{
	public void viselkedesszulo()
	{
		System.out.println("Szülő viselkedés");
	}
}

class Gyerek extends Szulo
{
	public void viselkedesgyerek()
	{
		System.out.println("Gyerek viselkedés");
	}
}

class szulogyerek
{
	
	public static void main(String[] args)
	{
		Szulo szulo = new Gyerek();
		szulo.viselkedesszulo();
		//szulo.viselkedesgyerek(); ezzel nem működik, mert a szülő típus nem kapja meg a gyerek viselkedését
	}
}
