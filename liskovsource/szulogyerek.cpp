#include<iostream>

using namespace std;


class Szulo
{
	public:
	void szviselkedes()
	{
		cout<<"Szülő viselkedés"<<endl;
	}
};

class Gyerek : public Szulo
{
	virtual void gyviselkedes()
	{
		cout<<"Gyerek viselkedés"<<endl;
	}
};

int main(int argc, char* argv[])
{
	Szulo szulo;
	//szulo.gyviselkedes(); ez nem működik
	szulo.szviselkedes();
}

