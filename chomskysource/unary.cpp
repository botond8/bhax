#include <iostream>

using namespace std;

int main()
{
	int a;
	int db = 0;
	cout<<"Egy decimalis szam: "<<endl;
	cin >> a;
	cout<<"Unaris szamrendszerben: "<<endl;
	for (int i = 0; i < a; ++i)
	{
		cout<<"|";
		++db;
		if(db%5==0)
        {    
            cout<<" ";
        }    
	}
	cout<<endl;
	return 0;
}
